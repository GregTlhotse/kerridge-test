﻿// See https://aka.ms/new-console-template for more information
using System.Globalization;

// Define tax rates
const double basicTaxRate = 0.1;
const double importDutyRate = 0.05;

// Rounding function for tax calculations
Func<double, double> roundTax = tax => Math.Ceiling(tax * 20) / 20;

// Process each shopping basket input
//string[] inputs = new string[] { "1 Book at 12.49", "1 Music CD at 14.99", "1 Chocolate bar at 0.85" };
//string[] inputs = new string[] { "1 Imported box of chocolates at 10.00", "1 Imported bottle of perfume at 47.50" };
string[] inputs = new string[] { "1 Imported bottle of perfume at 27.99", "1 Bottle of perfume at 18.99", "1 Packet of paracetamol at 9.75", "1 Box of imported chocolates at 11.25" };
for (int i = 0; i < inputs.Length; i += inputs.Length)
{
    // Initialize variables
    double total = 0;
    double totalTax = 0;

    // Process each item in the shopping basket
    for (int j = i; j < i + inputs.Length; j++)
    {
        string itemLine = inputs[j];
        string[] itemDetails = itemLine.Split(' ');

        // Extract quantity, name, and price
        int quantity = int.Parse(itemDetails[0]);
        string name = itemLine.Split(new string[] { " at " }, StringSplitOptions.None)[0];
        double price = double.Parse(itemDetails[itemDetails.Length - 1], CultureInfo.InvariantCulture);
        

        // Calculate basic tax (exempt for books, food, and medical)
        double basicTax = (name.ToLower().Contains("book") || name.ToLower().Contains("chocolate") || name.ToLower().Contains("paracetamol")) ? 0 : roundTax(price * basicTaxRate);

        // Calculate import duty
        double importDuty = name.ToLower().Contains("imported") ? roundTax(price * importDutyRate) : 0;

        // Calculate total tax for this item
        double itemTax = basicTax + importDuty;

        // Calculate total price for this item (quantity * price + tax)
        double itemTotal = quantity * (price + itemTax);

        // Print item details
        Console.WriteLine($"{name}: {itemTotal:0.00}");

        // Update running totals
        total += itemTotal;
        totalTax += itemTax;
    }

    // Print sales tax and total amount
    Console.WriteLine($"Sales Taxes: {totalTax:0.00}");
    Console.WriteLine($"Total: {total:0.00}");
    Console.WriteLine();
}
